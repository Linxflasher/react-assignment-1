import React, { Component } from 'react';
import './App.css';
import UserInput from "./components/UserInput/UserInput";
import UserOutput from "./components/UserOutput/UserOutput";

class App extends Component {

  state = {
    usernames: [
      {
        name: "Sergey"
      },
      {
        name: "Maximillian"
      }
    ]
  }

  switchUsernameHandler = (event) => {
    this.setState({
      usernames: [
        {
          name: event.target.value
        },
        {
          name: "Max"
        }
      ]
    })
  }

  render() {
    return (
      <div className="App">
        <UserInput change={this.switchUsernameHandler} />
        <UserOutput username={this.state.usernames[0].name} />
        <UserOutput username={this.state.usernames[1].name} />
      </div>
    );
  }
}

export default App;
