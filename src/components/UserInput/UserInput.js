import React from 'react';

const userInput = (props) => {
    return(
        <div className="user-input">
            <input type="text" onChange={props.change} defaultValue="Vuka" />
        </div>
    )
}

export default userInput;