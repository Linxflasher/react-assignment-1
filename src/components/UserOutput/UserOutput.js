import React from 'react';

const userOutput = (props) => {
    return(
        <div className="user-output">
            <p>User {props.username} Output 1</p>
            <p>User {props.username} Output 2</p>
        </div>
    )
}

export default userOutput;